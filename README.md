# XBRL Taxonomy Validation report

This repo holds the reports produced by the Logius XBRL tooling. 

The reports are pushed automatically after a taxonomy is validated.

The result is then automatically published on gitlab pages: https://xiffy.gitlab.io/xbrl-validation-reports/

## Docker en het Standaard platform
Op het Standaard Platfrom (SP) draait de XBRL-Taxonomieviewer (Nessie). Het is mogelijk om de raporten op hetzelfde domein te publiceren. 

Het SP is een kubernetes cluster. Dus om daar te kunnen functioneren moeten we een (docker-)image van de rapporten maken. Dat kan met: 
```shell
docker build -f Dockerfile -t xbrl-validation-reports .
```
daarmee bouwen op basis van de file `Dockerfile` een image vanuit de huidige directory. We taggen de image met `xbrl-validation-reports`. 

Je kan de image op je eigen systeem testen met
```shell
docker run --rm --name xbrl-validation_reports --publish 127.0.0.1:8080:8080 xbrl-validation_reports
``` 

Je kan nu met een browser naar http://localhost:8080/ en je ziet de index van de validatierapporten.

Wanneer je tevreden bent met wat je ziet is het tijd om te publiceren op de website. Daarvoor moet het zojuist aangemaakte image naar het SP worden gepusht

```shell
docker login harbor.cicd.s15m.nl
docker tag xbrl-validation-reports:latest  harbor.cicd.s15m.nl/bzk-xbrl/xbrl-validation-reports:v1.0.1
docker push harbor.cicd.s15m.nl/bzk-xbrl/xbrl-validation-reports:v1.0.1
```
Het image staat nu klaar om gepubliceerd te worden.
Het daadwerkelijk publiceren gaat door middel van een deployment. Dit deployment staat in de repo deploy-nessie op het standaard platform. 

n.b. de 'magie' is een nginx webserver die de html leest die we in de image hebben gekopieerd. De ingress van Nessie (https://taxonomie-viewer.logius.nl) forward alle traffic op /nta/validation-reports naar deze webserver, die sloopt /nta/validation-reports er weer af bij het lezen van schijf. Ook kan je dat lokaal testen; http://localhost:8080/nta/validation-reports/ laat dezelfde index zien.
